package de.itsocke.coronee.helper;

import android.Manifest;
import android.content.pm.PackageManager;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

public class PermissionRequest {
    public static final int REQUEST_ACCESS_COARSE_LOCATION = 1;

    public static boolean checkCoarseLocationPermission(FragmentActivity fragmentActivity){
        if(ContextCompat.checkSelfPermission(fragmentActivity,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(fragmentActivity,
                    new String[] {Manifest.permission.ACCESS_COARSE_LOCATION},
                    REQUEST_ACCESS_COARSE_LOCATION);
            return false;
        }
        else
            return true;
    }
}

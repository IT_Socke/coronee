package de.itsocke.coronee.helper.jobscheduler;

import android.annotation.SuppressLint;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.os.AsyncTask;
import java.util.ArrayList;
import de.itsocke.coronee.entities.Contact;
import de.itsocke.coronee.helper.db.DB_Source;
import de.itsocke.coronee.helper.notification.EthernetScan;

public class JobCheckContacts extends JobService {
    @Override
    public boolean onStartJob(JobParameters params) {
        new JobExecuter().execute();
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        return true;
    }

    @SuppressLint("StaticFieldLeak")
    private class JobExecuter extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            // Get DB Source
            DB_Source ds = new DB_Source(getApplicationContext());
            EthernetScan ethernetScan = new EthernetScan(getApplication());

            // Delete old Entries in DB
            ds.CONTACT_Delete();

            // Get current Entries in DB
            ArrayList<Contact> lContacs = ds.CONTACTS_SelectAll();

            // Check Contact to infected persons
            ethernetScan.hasContact(lContacs);

            // Rerun Job
            CheckContactsScheduler.scheduleJob(getApplicationContext());
            return null;
        }
    }
}
package de.itsocke.coronee.helper.jobscheduler;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class StartServiceReceiver extends BroadcastReceiver {
    @SuppressLint("UnsafeProtectedBroadcastReceiver")
    @Override
    public void onReceive(Context context, Intent intent) {
        CheckContactsScheduler.scheduleJob(context);
        FindContactsScheduler.scheduleJob(context);
    }
}
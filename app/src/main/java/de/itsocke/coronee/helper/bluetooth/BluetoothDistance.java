package de.itsocke.coronee.helper.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import java.util.Calendar;
import java.util.Date;
import de.itsocke.coronee.entities.Contact;
import de.itsocke.coronee.helper.db.DB_Source;
import de.itsocke.coronee.helper.notification.Notification;


public class BluetoothDistance {
    // Deklaration
    private BluetoothAdapter bluetoothAdapter;
    private Context context;
    private DB_Source ds;
    SharedPreferences preferences;


    // Constructor
    public BluetoothDistance(Context context){
        this.context = context;

        // Get BluetoothAdapter & DB Source
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        ds = new DB_Source(this.context.getApplicationContext());
        preferences = PreferenceManager.
                getDefaultSharedPreferences(this.context.getApplicationContext());
    }


    // Start Scanning
    public void start(){
        if(bluetoothAdapter != null && bluetoothAdapter.isEnabled())
            bluetoothAdapter.startDiscovery();
        else
            checkBluetoothState();

        context.registerReceiver(devicesFoundReceiver, new IntentFilter(BluetoothDevice.ACTION_FOUND));
        context.registerReceiver(devicesFoundReceiver, new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED));
    }


    // Activate Bluetooth
    private void checkBluetoothState(){
        if(bluetoothAdapter != null){
            if(!bluetoothAdapter.isEnabled()){
                BluetoothAdapter.getDefaultAdapter().enable();
                Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                enableIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(enableIntent);
                //fragmentActivity.startActivityForResult(enableIntent, REQUEST_ENABLE_BLUETOOTH);
            }
        }
    }


    // BroadcastReceiver for Save founded Devices
    private final BroadcastReceiver devicesFoundReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if(BluetoothDevice.ACTION_FOUND.equals(action)){
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                if(device != null) {
                    // Get RSSI and calculate the distance
                    int rssi        = intent.getShortExtra(BluetoothDevice.EXTRA_RSSI, Short.MIN_VALUE);
                    double distance = getDistance(rssi, -69);
                    //double distance2 = calculateDistance(rssi);

                    // Set Today with 0 Values
                    Calendar calendar = Calendar.getInstance();
                    calendar.set(Calendar.HOUR_OF_DAY, 0);
                    calendar.set(Calendar.MINUTE, 0);
                    calendar.set(Calendar.SECOND, 0);
                    calendar.set(Calendar.MILLISECOND, 0);
                    Date date = calendar.getTime();

                    // Add/Update Device in DB
                    ds.CONTACTE_CreateUpdateEntry(new Contact(device.getAddress(), date, distance));

                    // TODO Testzwecke
                    Notification noti = new Notification(context,
                            device.getName() + " - " + distance + "m",
                            "Found Device",
                            "Contact - Infection");
                    noti.doNotify();
                }
            }
            // Restart Discovery
            else if(BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                if(!preferences.getBoolean("killJobs", false)){
                    bluetoothAdapter.startDiscovery();
                }
            }
        }
    };


    // Calculate the Distance
    // txPower = -69 is the best Value
    private double getDistance(int rssi, int txPower) {
        double distance = Math.pow(10d, ((double) txPower - rssi) / (10 * 2));
        return  Math.round(distance * 100.0) / 100.0;
    }

    private double getDistance2(int rssi) {
        int txPower = -69; //hard coded power value. Usually ranges between -59 to -65

        if (rssi == 0) {
            return -1.0;
        }

        double ratio = rssi*1.0/txPower;
        if (ratio < 1.0) {
            return Math.pow(ratio,10);
        }
        else {
            return (0.89976)*Math.pow(ratio,7.7095) + 0.111;
        }
    }
}

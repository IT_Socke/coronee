package de.itsocke.coronee.helper.notification;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Build;

import androidx.core.app.NotificationCompat;

public class Notification {
    // Deklaration
    NotificationCompat.Builder builder;
    NotificationManager mNotificationManager;

    // Final
    final int notificationID = 123;
    final String channelID = "123";
    final String channelName = "Channel human readable title";

    public Notification(Context app, String title, String text, String subText){
        builder = new NotificationCompat.Builder(app, channelID)
                .setSmallIcon(android.R.drawable.ic_dialog_alert)
                .setContentTitle(title)
                .setContentText(text)
                .setSubText(subText);
                //.setPriority(NotificationCompat.PRIORITY_DEFAULT);

        mNotificationManager = (NotificationManager) app.getSystemService(Context.NOTIFICATION_SERVICE);

        // === Removed some obsoletes
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(
                    channelID, channelName,
                    NotificationManager.IMPORTANCE_HIGH);
            mNotificationManager.createNotificationChannel(channel);
        }
    }

    public void doNotify() {
        mNotificationManager.notify(notificationID, builder.build());
    }

}

# CoroNee

## Project description
The project topic is focused on Android programming with Bluetooth and Location.  The goal of your project work should be to contribute to pandemy-related questions like:
Recognizing violations of distance rules (e.g. by Bluetooth or Geofencing)
Protocollation/looging of of encounters with other persons
Indoor localization with Bluetooth low energy

This is only the description of the overall focus. In your project work you will have the opportunity to derive a more concrete project goal for your work by doing requirement analysis.

The project should contain the following parts: (1) An Android App which supports three scenarios which are related to my initial topics, (2) A project report (12pages) which describes the scenarios, the architectures of the app and some relevant implementations details.
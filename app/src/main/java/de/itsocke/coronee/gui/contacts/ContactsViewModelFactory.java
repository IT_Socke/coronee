package de.itsocke.coronee.gui.contacts;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class ContactsViewModelFactory implements ViewModelProvider.Factory {
    private FragmentActivity mApplication;


    public ContactsViewModelFactory(FragmentActivity application) {
        mApplication = application;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(ContactsViewModel.class)) {
            return (T) new ContactsViewModel(mApplication);
        }
        else
            throw new IllegalArgumentException("Unknown ViewModel class");
    }
}

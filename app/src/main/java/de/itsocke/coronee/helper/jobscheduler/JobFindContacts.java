package de.itsocke.coronee.helper.jobscheduler;

import android.annotation.SuppressLint;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.os.AsyncTask;
import de.itsocke.coronee.helper.bluetooth.BluetoothDistance;

public class JobFindContacts extends JobService {

    @Override
    public boolean onStartJob(JobParameters params) {
        new JobExecuter().execute();
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        return true;
    }

    @SuppressLint("StaticFieldLeak")
    private class JobExecuter extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            // Find Device Bluetooth
            BluetoothDistance blSearch = new BluetoothDistance(getApplication());
            blSearch.start();

            // ReRun Job
            //FindContactsScheduler.scheduleJob(getApplication());
            return null;
        }
    }
}
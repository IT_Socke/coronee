package de.itsocke.coronee.gui.home;

import android.app.job.JobScheduler;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import de.itsocke.coronee.R;
import de.itsocke.coronee.entities.Contact;
import de.itsocke.coronee.helper.PermissionRequest;
import de.itsocke.coronee.helper.db.DB_Source;
import de.itsocke.coronee.helper.jobscheduler.CheckContactsScheduler;
import de.itsocke.coronee.helper.jobscheduler.FindContactsScheduler;
import de.itsocke.coronee.helper.notification.EthernetScan;

import static android.content.Context.JOB_SCHEDULER_SERVICE;

public class HomeFragment extends Fragment {
    // Deklaration
    SharedPreferences preferences;
    DB_Source ds;
    private EthernetScan ethernetScan;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferences = PreferenceManager.
                getDefaultSharedPreferences(this.getActivity());
        ethernetScan = new EthernetScan(requireActivity().getApplication());
        ds = new DB_Source(getActivity());
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Get Button
        //private TextView textView;
        Button btnStart     = view.findViewById(R.id.btnActivate);
        Button btnStop      = view.findViewById(R.id.btnStop);
        Button btnCheck     = view.findViewById(R.id.btnCheck);
        Button btnInfected  = view.findViewById(R.id.btnInfected);

        // Set OnClickListener
        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startJobs();
            }
        });
        btnStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                killJobs();
            }
        });
        btnCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Get current Entries in DB
                ArrayList<Contact> lContacs = ds.CONTACTS_SelectAll();
                ethernetScan.hasContact(lContacs);
            }
        });
        btnInfected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ethernetScan.setInfected();
            }
        });
    }


    private void startJobs(){
        // Kill all Jobs before
        JobScheduler scheduler = (JobScheduler) this.requireActivity().getSystemService(JOB_SCHEDULER_SERVICE);
        scheduler.cancelAll();

        // Start Check Job
        CheckContactsScheduler.scheduleJob(this.requireActivity().getApplicationContext());

        // Check Permissions and then start FindContactsJob
        if(PermissionRequest.checkCoarseLocationPermission(this.getActivity()))
            FindContactsScheduler.scheduleJob(this.requireActivity().getApplicationContext());

        // Set Alive for Bluetooth
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("killJobs", false);
        editor.apply();
    }


    private void killJobs(){
        // Kill all Jobs
        JobScheduler scheduler = (JobScheduler) this.requireActivity().getSystemService(JOB_SCHEDULER_SERVICE);
        scheduler.cancelAll();

        // Set Kill for Bluetooth
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("killJobs", true);
        editor.apply();
    }
}
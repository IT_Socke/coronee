package de.itsocke.coronee.gui.contacts;

import java.util.ArrayList;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModel;
import de.itsocke.coronee.entities.Contact;
import de.itsocke.coronee.helper.db.DB_Source;


public class ContactsViewModel extends ViewModel {
    // Deklaration
    ContactRowAdapter contactRowAdapter;

    public ContactsViewModel(FragmentActivity application) {
        // Get DB -> get Contacts
        DB_Source ds = new DB_Source(application.getApplicationContext());
        ArrayList<Contact> lContacts = ds.CONTACTS_SelectAll();

        // Create RowAdapter
        contactRowAdapter = new ContactRowAdapter(application, lContacts);
    }


    public ContactRowAdapter getAdapter() {
        return contactRowAdapter;
    }
}
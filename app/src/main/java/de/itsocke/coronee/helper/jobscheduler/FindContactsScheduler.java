package de.itsocke.coronee.helper.jobscheduler;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;


import static android.content.Context.JOB_SCHEDULER_SERVICE;

public class FindContactsScheduler {
    public static void scheduleJob(Context context) {
        // Set Latency 0 to start immediately
        long lLatency = 0;

        // Create JobScheduler
        JobScheduler jobScheduler = (JobScheduler)context.getSystemService(JOB_SCHEDULER_SERVICE);
        ComponentName componentName = new ComponentName(context, JobFindContacts.class);
        JobInfo jobInfo = new JobInfo.Builder(0, componentName)
                .setMinimumLatency(lLatency)
                .setOverrideDeadline(lLatency)
                .setPersisted(true)
                .build();
        jobScheduler.schedule(jobInfo);
    }
}
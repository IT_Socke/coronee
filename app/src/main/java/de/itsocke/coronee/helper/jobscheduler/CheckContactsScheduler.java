package de.itsocke.coronee.helper.jobscheduler;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;

import java.util.Calendar;

import static android.content.Context.JOB_SCHEDULER_SERVICE;

public class CheckContactsScheduler {
    public static void scheduleJob(Context context) {
        // Set Calendar to 6pm
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 18);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        // Calculate the Timedistance to 6pm
        long lLatency = calendar.getTimeInMillis() - System.currentTimeMillis();


        // after 6pm ? -> calculate to 6pm tomorrow
        if(lLatency < 0){
            calendar.add(Calendar.HOUR, 24);
            lLatency = calendar.getTimeInMillis() - System.currentTimeMillis();
        }

        // Create JobScheduler
        JobScheduler jobScheduler = (JobScheduler)context.getSystemService(JOB_SCHEDULER_SERVICE);
        ComponentName componentName = new ComponentName(context, JobCheckContacts.class);
        JobInfo jobInfo = new JobInfo.Builder(1, componentName)
                .setMinimumLatency(lLatency)
                .setOverrideDeadline(lLatency)
                .setPersisted(true)
                .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                .build();
        jobScheduler.schedule(jobInfo);
    }
}
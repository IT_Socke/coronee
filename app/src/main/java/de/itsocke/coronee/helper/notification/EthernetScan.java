package de.itsocke.coronee.helper.notification;

import android.annotation.SuppressLint;
import android.app.Application;
import android.bluetooth.BluetoothAdapter;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import java.net.URLEncoder;
import java.util.ArrayList;
import de.itsocke.coronee.entities.Contact;

public class EthernetScan {
    // Deklaration
    private Application application;

    // Final
    private final String serverIP = "12.43.48.59";

    public EthernetScan(Application application){
        this.application = application;
    }


    @SuppressLint("HardwareIds")
    public void setInfected() {
        // Deklaration
        String address;
        String url;
        RequestQueue requestQueue = Volley.newRequestQueue(application);
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        // Get BluetoothAdapter Address
        if(bluetoothAdapter != null && bluetoothAdapter.isEnabled())
            address = bluetoothAdapter.getAddress();
        else
            return;

        // Build URL
        try {
            url = serverIP + "?" + URLEncoder.encode("setInfected", "UTF-8")
                    + "=" + URLEncoder.encode(address, "UTF-8");
        }
        catch (Exception ex){
            return;
        }

        // Build request
        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Notification noti = new Notification(application, "You are Infected",
                        "You set yourself to infected.",
                        "Contact - Infection");
                noti.doNotify();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Notification noti = new Notification(application,
                        "Error set yourself infected",
                        "Could not set yourself to infected.",
                        "Contact - Infection");
                noti.doNotify();
            }
        });

        requestQueue.add(request);
    }


    public void hasContact(ArrayList<Contact> lContacts) {
        // Deklaration
        String data = "";
        String url;
        RequestQueue requestQueue = Volley.newRequestQueue(application);

        // Build POST value
        try {
            StringBuilder value = new StringBuilder();
            for (Contact contact: lContacts) {
                value.append(contact.getAddress()).append(";");
            }

            if(value.length() > 0) {
                data = URLEncoder.encode("getInfected", "UTF-8")
                        + "=" + URLEncoder.encode(value.substring(0, value.length() - 1), "UTF-8");
            }
        }
        catch (Exception ex){
            return;
        }

        // Check if Contacts exist
        if(!data.isEmpty())
            url = serverIP + "?" + data;
        else {
            Notification noti = new Notification(application, "No Contacts to check",
                    "You don't have any contacts.",
                    "No Contacts");
            noti.doNotify();
            return;
        }

        // Build request
        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(response.equals("true")){
                    Notification noti = new Notification(application, "Contact - Infection",
                            "You had contact to an infected person.",
                            "Contact to infected Person");
                    noti.doNotify();
                }
            }
        }, new Response.ErrorListener() { //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {
                Notification noti = new Notification(application, "Error by checking Contacts",
                        "Could not check the contacts.",
                        "Error check Contacts");
                noti.doNotify();
            }
        });

        requestQueue.add(request);
    }
}

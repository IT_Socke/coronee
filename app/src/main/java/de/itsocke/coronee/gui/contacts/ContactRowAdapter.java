package de.itsocke.coronee.gui.contacts;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import de.itsocke.coronee.R;
import de.itsocke.coronee.entities.Contact;

public class ContactRowAdapter extends RecyclerView.Adapter<ContactRowAdapter.MyViewHolder> {
    // Deklaration
    ArrayList<Contact> lContacts;
    Context context;

    public ContactRowAdapter(Context context, ArrayList<Contact> lContacts){
        this.context = context;
        this.lContacts = lContacts;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.contact_row, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.tvAddress.setText(lContacts.get(position).getAddress());
        holder.tvDistance.setText(lContacts.get(position).getDistanceString());
        holder.ivAttention.setImageResource(android.R.drawable.ic_dialog_alert);
    }

    @Override
    public int getItemCount() {
        return lContacts.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder{
        // Deklaration
        TextView tvAddress;
        TextView tvDistance;
        ImageView ivAttention;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            // Set UI elements
            tvAddress = itemView.findViewById(R.id.tvAddress);
            tvDistance = itemView.findViewById(R.id.tvDistance);
            ivAttention = itemView.findViewById(R.id.ivAttention);
        }
    }
}

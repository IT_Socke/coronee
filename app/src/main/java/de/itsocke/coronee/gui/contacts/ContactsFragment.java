package de.itsocke.coronee.gui.contacts;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import de.itsocke.coronee.R;

public class ContactsFragment extends Fragment {
    // Deklaration
    private ContactsViewModel contactsViewModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        contactsViewModel = new ViewModelProvider(this,
                new ContactsViewModelFactory(this.getActivity())).get(ContactsViewModel.class);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_contacts, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Get RecyclerView and set RowAdapter
        RecyclerView recyclerView = view.findViewById(R.id.recyclerview);
        ContactRowAdapter contactRowAdapter = contactsViewModel.getAdapter();
        recyclerView.setAdapter(contactRowAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    }
}
package de.itsocke.coronee.entities;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class Contact {
    // Deklaration
    private String address;
    private Date date;
    private double distance;

    public Contact(String address, String date, double distance){
        setAddress(address);
        setDistance(distance);
        setDate(date);
    }
    public Contact(String address, Date date, double distance){
        setAddress(address);
        setDistance(distance);
        setDate(date);
    }


    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }


    public double getDistance() {
        return distance;
    }
    public String getDistanceString() {
        return getDistance() + "m";
    }
    public void setDistance(double distance) {
        this.distance = distance;
    }


    public String getDate() {
        SimpleDateFormat simpleDate =  new SimpleDateFormat("dd.MM.yyyy", Locale.GERMAN);
        return simpleDate.format(date);
    }
    public void setDate(String sDate) {
        Date date;
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy", Locale.GERMANY);

        try {
            date = format.parse(sDate);
        } catch (ParseException e) {
            date = Calendar.getInstance().getTime();
        }

        this.date = date;
    }
    public void setDate(Date date) {
        this.date = date;
    }
}

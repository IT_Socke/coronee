package de.itsocke.coronee.helper.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.NonNull;

public class SQLiteHelper extends SQLiteOpenHelper {
    // Final Strings
    private static final String DATABASE_NAME = "planer.db";
    private static final int DATABASE_VERSION = 1;

    //region Create Strings
    private static final String TABLE_CREATE_CONTACT = ""
            +   "CREATE TABLE IF NOT EXISTS CONTACT("
            +   "ADDRESS text, "
            +   "DATE text, "
            +   "DISTANCE numeric)";

    //endregion

    //region Drop Strings
    private static final String TABLE_DROP_CONTACT = ""
            +"DROP TABLE IF EXISTS CONTACT";
    //endregion

    public SQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(@NonNull SQLiteDatabase database) {
        database.execSQL(TABLE_CREATE_CONTACT);
    }

    public void resetDB(@NonNull SQLiteDatabase database) {
        // Delete & Recreate Table
        database.execSQL(TABLE_DROP_CONTACT);
        onCreate(database);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) { }
}

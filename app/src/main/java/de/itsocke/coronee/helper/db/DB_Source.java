package de.itsocke.coronee.helper.db;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import androidx.annotation.NonNull;
import de.itsocke.coronee.entities.Contact;


public class DB_Source {
    // Deklaration
    private SQLiteDatabase database;
    private SQLiteHelper dbHelper;

    // Konstruktor
    public DB_Source(Context context) {
        dbHelper = new SQLiteHelper(context);
    }

    // Columns
    private String[] columnsContact = {"ADDRESS", "DATE", "DISTANCE"};

    // Open DB
    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    // Close DB
    public void close() {
        dbHelper.close();
    }

    // Select All
    public ArrayList<Contact> CONTACTS_SelectAll(){
        // Deklaration
        ArrayList<Contact> lEntries = new ArrayList<>();
        Contact contact;
        int anzahl;

        // Open DB
        open();

        // Create Cursor
        @SuppressLint("Recycle") Cursor cursor = database.query("CONTACT", columnsContact,
                null, null, null, null, "DATE");

        // Get Number of Entries
        if(cursor.getCount() == 0) return lEntries;
        anzahl = cursor.getCount();

        // Read Entries
        for (int i = 0; i < anzahl; i++) {
            cursor.moveToNext();
            contact = new Contact(cursor.getString(0), cursor.getString(1),
                    cursor.getDouble(2));
            lEntries.add(contact);
        }

        cursor.close();

        return lEntries;
    }

    // Create/Update
    @SuppressLint("Recycle")
    public void CONTACTE_CreateUpdateEntry(@NonNull Contact contact) {
        // Deklaration
        Cursor cursor;
        double currentDistance;
        ContentValues values = new ContentValues();
        values.put("ADDRESS", contact.getAddress());
        values.put("DATE", contact.getDate());
        values.put("DISTANCE", contact.getDistance());

        // Open DB
        open();

        // DS updaten falls vorhanden
        cursor = database.query("CONTACT", columnsContact, "ADDRESS='" + contact.getAddress()
                        + "' AND DATE='" + contact.getDate() + "'",
                null, null, null, null);

        // Update or insert
        if(cursor.getCount() > 0) {
            cursor.moveToNext();
            currentDistance = cursor.getDouble(2);

            // Only update when distance is lower
            if (currentDistance > contact.getDistance()) {
                database.update("CONTACT", values, "ADDRESS='" + contact.getAddress()
                        + "' AND DATE='" + contact.getDate() + "'", null);
            }
        }
        else
            database.insert("CONTACT", null, values);

        // Close DB
        close();
    }

    // Delete Entries older then 30 days
    public void CONTACT_Delete(){
        // Calc day -30
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -30);
        Date dateBefore30Days = cal.getTime();

        // Open DB
        open();

        // Delete Entries
        database.delete("CONTACT", "DATE < '" + dateBefore30Days + "'", null);

        // Close DB
        close();
    }
}
